FROM python:3.10



RUN mkdir /warehouse

WORKDIR /warehouse

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

LABEL version="2.0" maintainer="NicKSarD <n.makovenko@zs.ttk.ru>"

RUN chmod a+x /warehouse/docker/*.sh

CMD ["gunicorn", "my_main:app", "--workers", "4", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind=0.0.0.0:8000"]

