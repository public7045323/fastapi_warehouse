from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.exceptions import HTTPException

from app.config.database import engine
from app.users.router import router as router_users
from app.main.router import router as router_main
from app.warehouse.router import router as router_warehouse
from app.admin.views import UserAdmin, WarehouseAdmin
from app.admin.auth import authentication_backend
from app.config.config import settings
from app.config.flash import flash, get_flashed_messages

from sqladmin import Admin

from starlette.middleware import Middleware
from starlette.middleware.sessions import SessionMiddleware
import uvicorn




middleware = [
    Middleware(SessionMiddleware, secret_key=settings.SECRET_KEY)
]


app = FastAPI(middleware=middleware)
app.include_router(router_users)
app.include_router(router_main)
app.include_router(router_warehouse)
app.mount('/static', StaticFiles(directory='app/static'), name='static')
templates = Jinja2Templates(directory="app/templates")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@app.exception_handler(403)
async def exc403handler(request: Request, exc: HTTPException):
    return RedirectResponse('/403')

@app.get('/403', response_class=HTMLResponse)
async def exc403(request:Request):
    flash(request, 'Ваш токен истек, войдите на сайт.', 'alert-danger')
    return RedirectResponse('auth/signin')


@app.exception_handler(404)
async def exc404handler(request: Request, exc: HTTPException):
    return RedirectResponse("/404")

@app.get('/404', response_class=HTMLResponse)
async def exc404(request:Request):
    data_main = {'title': '404'}
    return templates.TemplateResponse("main/404.html", {"request":request, 'data_main': data_main})




admin = Admin(app, engine, authentication_backend=authentication_backend)
admin.add_view(WarehouseAdmin)
admin.add_view(UserAdmin)




if __name__ == '__main__':
    uvicorn.run("my_main:app", host="192.168.174.130", port=8000, log_level="info", reload=True, workers=4)