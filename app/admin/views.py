from sqladmin import ModelView
from app.users.models import User
from app.warehouse.models import Warehouse




class UserAdmin(ModelView, model=User):
    column_list = [
        User.id, 
        User.name, 
        User.email, 
        User.access,
        User.is_active,
        User.is_admin, 
        User.registered_at
    ]
    # column_details_exclude_list=[User.hashed_password]
    can_delete = False
    name = 'Пользователь'
    name_plural = 'Пользователи'
    icon = 'fa-solid fa-user'
    page_size = 20
    page_size_options = [10, 25, 50, 100]
    column_searchable_list = [User.name, User.email]
    column_sortable_list = [
        User.id,
        User.name,
        User.email,
        User.access,
        User.is_admin,
        User.registered_at
    ]
    column_default_sort = ("id", True)
    column_labels = {
        User.id: "ID",
        User.name: "Name", 
        User.email: "Email"
    }
    can_export = False


class WarehouseAdmin(ModelView, model=Warehouse):
    column_list = [
        Warehouse.id, 
        Warehouse.name, 
        Warehouse.quantity, 
        Warehouse.desc_in,
        Warehouse.desc_out,
        Warehouse.status, 
        Warehouse.created_at,
        Warehouse.updated_at,
    ]

    can_delete = True
    name = 'Склад'
    name_plural = 'Склад'
    icon = 'fa-solid fa-cube'
    page_size = 20
    page_size_options = [10, 25, 50, 100]
    column_searchable_list = [Warehouse.name, Warehouse.desc_in, Warehouse.desc_out]
    column_sortable_list = [
        Warehouse.id, 
        Warehouse.name, 
        Warehouse.quantity, 
        Warehouse.desc_in,
        Warehouse.desc_out,
        Warehouse.status, 
        Warehouse.created_at,
        Warehouse.updated_at,
    ]
    column_default_sort = ("id", True)
    column_labels = {
        Warehouse.id: "ИД",
        Warehouse.name: "Наименование", 
        Warehouse.quantity: 'Количество',
        Warehouse.desc_in: 'Описание вх',
        Warehouse.desc_out: 'Описание вых',
        Warehouse.status: 'Статус',
        Warehouse.created_at: 'Дата создания',
        Warehouse.updated_at: 'Дата редактирования',
    }
    can_export = True


