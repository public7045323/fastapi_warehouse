from app.config.dao.base import BaseDAO
from app.warehouse.models import Warehouse
from sqlalchemy import or_, select,  insert, delete, func, desc, update
import math




class WarehouseDAO(BaseDAO):
    model = Warehouse

    @classmethod
    async def paginate_all(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate
        
        
    @classmethod
    async def find_all_or(cls, q):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(or_(
                cls.model.__table__.columns.name.ilike(f'%{q}%'),
                cls.model.__table__.columns.desc_in.ilike(f'%{q}%'),
                cls.model.__table__.columns.desc_out.ilike(f'%{q}%'),
                )
            )
            result_data = await session.execute(query)
            result = result_data.all()

            return result