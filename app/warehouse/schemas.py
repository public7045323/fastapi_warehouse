from pydantic import BaseModel
from fastapi import Form



class SWarehouse_new(BaseModel):
    name: str
    quantity: int
    desc_in: str
    status: bool
    
    @classmethod
    def as_form(
        cls,
        name: str = Form(...),
        quantity: int = Form(...),
        desc_in: str = Form(...),
        status: bool = Form(0),
    ):
        return cls(
            name=name,
            quantity=quantity,
            desc_in=desc_in,
            status = status,
        )
    
    class Config:
        from_attibutes = True


class SWarehouse_edit(BaseModel):
    name: str
    quantity: int
    desc_in: str
    desc_out: str
    status: bool

    
    @classmethod
    def as_form(
        cls,
        name: str = Form(...),
        quantity: int = Form(...),
        desc_in: str = Form(...),
        desc_out: str = Form(...),
        status: bool = Form(0),
    ):
        return cls(
            name=name,
            quantity=quantity,
            desc_in=desc_in,
            desc_out=desc_out,
            status = status,
        )
    
    class Config:
        from_attibutes = True