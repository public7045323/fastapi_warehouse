from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime



class Warehouse(Base):
    __tablename__ = "warehouse"
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    name: Mapped[str] = mapped_column(nullable=False)
    quantity: Mapped[int] = mapped_column(nullable=False)
    desc_in: Mapped[str] = mapped_column(nullable=True)
    desc_out: Mapped[str] = mapped_column(nullable=True)
    status: Mapped[bool] = mapped_column(nullable=False, default=True)
    created_at: Mapped[datetime] = mapped_column(server_default=func.now())
    updated_at: Mapped[datetime] = mapped_column(onupdate=func.now(), nullable=True)

    def __str__(self):
        return f'Warehouse: {self.name}'
