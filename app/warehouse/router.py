from typing import Annotated
from fastapi import APIRouter, Depends, HTTPException, Path, Request, Response, status
from fastapi.params import Query
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from app.config.access import site_access
from app.users.dependencies import get_current_user
from app.users.models import User
from app.config.flash import flash, get_flashed_messages
from app.warehouse.dao import WarehouseDAO
from app.warehouse.schemas import SWarehouse_edit, SWarehouse_new



router = APIRouter(prefix='/warehouse', tags=['Warehouse'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages



@router.get("/",)
async def warehouse_main_get(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=15),
):
    access = await site_access(request, current_user, 'warehouse_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main = {'title': 'Склад'}
    
    paginate = await WarehouseDAO.paginate_active(page, size)
    if paginate is None:
        flash(request, 'Склад пуст.', 'alert-info')

    return templates.TemplateResponse(
        'warehouse/warehouse_index.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )


@router.get("/all",)
async def warehouse_all_get(
    request: Request, 
    current_user: User = Depends(get_current_user),
    page: int = Query(ge=0, default=1), 
    size: int = Query(ge=1, le=50, default=15),
):
    access = await site_access(request, current_user, 'warehouse_view')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_main = {'title': 'Склад все'}
    
    paginate = await WarehouseDAO.paginate_all(page, size)
    if paginate is None:
        flash(request, 'Нет активных задач.', 'alert-info')

    return templates.TemplateResponse(
        'warehouse/warehouse_all.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'paginate': paginate}
    )


@router.get("/edit/{warehouse_id}")
async def warehouse_edit_get(
    warehouse_id: Annotated[int, Path(ge=1,)],
    request:Request,
    current_user: User = Depends(get_current_user),
):
    data_main = {'title': 'Редактирование позиции'}
    
    access = await site_access(request, current_user, 'warehouse_edit')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    data_base = await WarehouseDAO.find_by_id(warehouse_id)
    
    return templates.TemplateResponse(
        "warehouse/warehouse_edit.html", 
        {"request":request, 'user': current_user, 'data_main': data_main, 'data_base': data_base}
    )


@router.post('/edit/{warehouse_id}')
async def warehouse_edit_post(
    warehouse_id: Annotated[int, Path(ge=1,)],
    request: Request, 
    current_user: User = Depends(get_current_user), 
    data_form: SWarehouse_edit=Depends(SWarehouse_edit.as_form),
):
    access = await site_access(request, current_user, 'warehouse_edit')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    await WarehouseDAO.update(
        id = warehouse_id,
        name = data_form.name,
        quantity = data_form.quantity,
        desc_in = data_form.desc_in,
        desc_out = data_form.desc_out,
        status = data_form.status,
    )
    
    return RedirectResponse(request.url_for('warehouse_main_get'), status_code=303)


@router.get('/new',)
async def warehouse_new_get(request:Request, current_user: User = Depends(get_current_user)):
    data_main = {'title': 'Добавление на склад'}
    
    access = await site_access(request, current_user, 'warehouse_add')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
    
    return templates.TemplateResponse("warehouse/warehouse_new.html", {"request":request, 'user': current_user, 'data_main': data_main})


@router.post('/new')
async def warehouse_new_post(
    request: Request, 
    current_user: User = Depends(get_current_user), 
    data_task: SWarehouse_new=Depends(SWarehouse_new.as_form)
):
    access = await site_access(request, current_user, 'warehouse_add')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
        
    await WarehouseDAO.add(
        name = data_task.name,
        quantity = data_task.quantity, 
        desc_in = data_task.desc_in, 
    )

    return RedirectResponse(request.url_for('warehouse_new_get'), status_code=303)


@router.get('/search/')
async def warehouse_search_post(
    request: Request, 
    current_user: User = Depends(get_current_user),
    q: str | None = None, 
):
    data_main = {'title': 'Поиск по складу'}
    access = await site_access(request, current_user, 'warehouse_add')
    if access.get('status') == None:
        return RedirectResponse(request.url_for(access.get('path')))
        
    if not q:
        return RedirectResponse(request.url_for('warehouse_all_get'), status_code=303)
    
    data_base = await WarehouseDAO.find_all_or(q)
    if data_base == []:
        flash(request, f'По вашему запросу {q} ничего не найдено.', 'alert-warning')
        return RedirectResponse(request.url_for('warehouse_all_get'))

    return templates.TemplateResponse(
        'warehouse/warehouse_search.html', 
        {'request': request, 'data_main': data_main, 'user': current_user, 'data_base': data_base}
    )