from app.config.dao.base import BaseDAO
from app.users.models import User
from sqlalchemy import or_, select,  insert, delete, func, desc, update




class UsersDAO(BaseDAO):
    model = User

    @classmethod
    async def update_pass(cls, email, **data):
        async with cls.async_session_maker() as session:
            query = update(cls.model.__table__).where(cls.model.__table__.columns.email == email).values(**data)
            try:
                await session.execute(query)
                await session.commit()
            except:
                await session.rollback()

