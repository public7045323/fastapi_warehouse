from sqlalchemy import Column, Integer, String, Boolean, TIMESTAMP
from sqlalchemy.orm import relationship, Mapped, mapped_column
from sqlalchemy.types import Text
from sqlalchemy.sql import func
from app.config.database import Base
from datetime import datetime



class User(Base):
    __tablename__ = "users"
    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    name: Mapped[str] = mapped_column(Text, nullable=False)
    email: Mapped[str] = mapped_column(Text, nullable=False, unique=True)
    hashed_password: Mapped[str] = mapped_column(Text, nullable=False)
    access: Mapped[str] = mapped_column(Text, nullable=True)
    is_active: Mapped[bool] = mapped_column(nullable=True, default=False)
    is_admin: Mapped[bool] = mapped_column(nullable=True, default=False)
    registered_at: Mapped[datetime] = mapped_column(server_default=func.now())


    def __str__(self):
        return f'User: {self.email}'
