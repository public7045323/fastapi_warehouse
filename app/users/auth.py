from jose import jwt
from passlib.context import CryptContext
from pydantic import EmailStr

from app.users.dao import UsersDAO
from app.config.config import settings
from datetime import datetime, timedelta
from time import time




pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def create_access_token(data: dict) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(weeks=2)
    # expire = datetime.utcnow() + timedelta(minutes=1)

    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode, settings.SECRET_KEY, settings.ALGORITHM
    )
    return encoded_jwt


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


async def authenticate_user(email: EmailStr, password: str):
    user = await UsersDAO.find_one_or_none(email=email)
    if not user:
        return None
    elif not verify_password(password, user.hashed_password):
        return None
    
    return user


####### Pass reset.
async def get_password_reset_token(id, expires_in=600) -> str:
    return jwt.encode(
        {'password_reset_id': id, 'exp': time()+ expires_in},
        settings.SECRET_KEY, 
        settings.ALGORITHM
    )


async def verify_password_reset_token(token_reset):
    try:
        id = jwt.decode(token_reset, 
                        settings.SECRET_KEY,
                        settings.ALGORITHM,
                        )['password_reset_id']
    except:
        return None
    return id








