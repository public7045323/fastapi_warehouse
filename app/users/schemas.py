from pydantic import BaseModel, EmailStr
from fastapi import Form



class SUsersAuthIn(BaseModel):
    email: EmailStr
    password: str
    
    @classmethod
    def as_form(
        cls,
        email: EmailStr = Form(...),
        password: str = Form(...)
    ):
        return cls(
            email=email,
            password=password
        )
    
    class Config:
        from_attibutes = True


class SUsersAuthUp(BaseModel):
    name: str
    email: EmailStr
    password: str
    password2: str
    
    @classmethod
    def as_form(
        cls,
        name: str = Form(...),
        email: EmailStr = Form(...),
        password: str = Form(...),
        password2: str = Form(...),
    ):
        return cls(
            name=name,
            email=email,
            password=password,
            password2=password2,
        )
    
    class Config:
        from_attibutes = True


class SPassChange(BaseModel):
    name: str
    email: EmailStr
    password_now: str
    password: str
    password2: str
    
    @classmethod
    def as_form(
        cls,
        name: str = Form(...),
        email: EmailStr = Form(...),
        password_now: str = Form(...),
        password: str = Form(...),
        password2: str = Form(...),
    ):
        return cls(
            name=name,
            email=email,
            password_now=password_now,
            password=password,
            password2=password2,
        )
    
    class Config:
        from_attibutes = True


class SPassReset(BaseModel):
    email: EmailStr

    @classmethod
    def as_form(
        cls,
        email: EmailStr = Form(...)
    ):
        return cls(
            email=email,
        )
    
    class Config:
        from_attibutes = True


class SPassResetId(BaseModel):
    password: str
    password2: str
    
    @classmethod
    def as_form(
        cls,
        password: str = Form(...),
        password2: str = Form(...),
    ):
        return cls(
            password=password,
            password2=password2,
        )
    
    class Config:
        from_attibutes = True