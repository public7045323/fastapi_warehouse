from fastapi import APIRouter, Depends, Request, Response
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from app.config.flash import get_flashed_messages
from app.users.dependencies import get_current_user

from app.users.models import User



router = APIRouter( prefix='', tags=['Main'])
templates=Jinja2Templates(directory="app/templates/")
templates.env.globals['get_flashed_messages'] = get_flashed_messages


@router.get("/", response_class=HTMLResponse)
@router.get("/index", response_class=HTMLResponse)
async def index_get(request: Request, current_user: User = Depends(get_current_user)):
    data_main= {'title': 'Main'}
    
    return templates.TemplateResponse('main/index.html', {'request': request, 'data_main': data_main, 'user': current_user})


@router.get('/about', response_class=HTMLResponse)
async def about_get(request: Request, current_user: User = Depends(get_current_user)):
    data_main = {'title': 'About'}
    
    return templates.TemplateResponse('main/about.html', {'request': request, 'data_main': data_main, 'user': current_user})




