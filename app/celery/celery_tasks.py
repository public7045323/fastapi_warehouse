import smtplib

from pydantic import EmailStr
from app.celery.celery import celery
from app.config.config import settings
from app.celery.email_templates import send_email_template


@celery.task
def send_email_pass_reset(
    www: str,    
    link_reset: str,
    email_to: EmailStr,
):
    msg_content = send_email_template(
        www=www, 
        link_reset=link_reset, 
        email_to=email_to)
    
    with smtplib.SMTP_SSL(settings.SMTP_HOST, settings.SMTP_PORT) as server:
        server.login(settings.SMTP_USER, settings.SMTP_PASS)
        server.send_message(msg_content)


