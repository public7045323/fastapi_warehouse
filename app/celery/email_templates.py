from email.message import EmailMessage
import smtplib
from app.config.config import settings
from pydantic import EmailStr



def send_email_template(
    www: str,    
    link_reset: str,
    email_to: EmailStr,
):
    email = EmailMessage()
    email['Subject'] = 'Запрос на сброс пароля.'
    email['From'] = settings.SMTP_USER
    email['To'] = email_to
    email.set_content(
        f"""
            <h2> Запрос на сброс пароля на сайте {www}. </h2> 
            <p> Для сброса пароля перейдите по ссылке: </p>
            <p> {link_reset} </p>
            <p> ----------------------------------------------</p>
            <p> Если вы не запрашивали сброс пароля, то проигнорируйте это письмо. </p>
        """,
        subtype='html'
    )
    return email
    # with smtplib.SMTP_SSL(settings.SMTP_HOST, settings.SMTP_PORT) as server:
    #     server.login(settings.SMTP_USER, settings.SMTP_PASS)
    #     server.send_message(email)