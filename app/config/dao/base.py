import math
from app.config.database import async_session_maker
from sqlalchemy import select,  insert, delete, func, desc, update
from sqlalchemy.exc import SQLAlchemyError




class BaseDAO:
    model = None
    async_session_maker = async_session_maker

    @classmethod
    async def find_by_id(cls, model_id: int):
        async with cls.async_session_maker() as session:
            query = select(cls.model).filter_by(id=model_id)
            result = await session.execute(query)
            return result.scalar_one_or_none()
    
    @classmethod
    async def find_one_or_none(cls, **filter_by):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).filter_by(**filter_by)
            result = await session.execute(query)
            return result.mappings().one_or_none()
    
    @classmethod
    async def find_all(cls, **filter_by):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).filter_by(**filter_by).order_by(desc(cls.model.__table__.columns.id))
            result = await session.execute(query)
            return result.all()
    
    @classmethod
    async def add(cls, **data):
        try:
            async with cls.async_session_maker() as session:
                query = insert(cls.model).values(**data)
                await session.execute(query)
                await session.commit()
        except Exception as ex:
            print(ex)
    
    @classmethod
    async def delete(cls, **filter_by):
        async with cls.async_session_maker() as session:
            query = delete(cls.model).filter_by(**filter_by)
            await session.execute(query)
            await session.commit()
    
    @classmethod
    async def update(cls, id, **data):
        async with cls.async_session_maker() as session:
            query = update(cls.model.__table__).where(cls.model.__table__.columns.id == id).values(**data)
            try:
                await session.execute(query)
                await session.commit()
            except:
                await session.rollback()
    
    @classmethod
    async def paginate(cls, page, size):
        async with cls.async_session_maker() as session:
            query = select(cls.model.__table__.columns).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate
    
    @classmethod
    async def paginate_active(cls, page, size):
        async with async_session_maker() as session:
            query = select(cls.model.__table__.columns).where(cls.model.__table__.columns.status == True).order_by(desc(cls.model.__table__.columns.id)).offset((page-1)*size).limit(size)
            result_data = await session.execute(query)
            data = result_data.all()
            if not data:
                return None
            
            query_all = select(func.count()).select_from(cls.model.__table__).where(cls.model.__table__.columns.status == True)
            result_all = await session.execute(query_all)
            total_records = result_all.scalar() or 0
            total_pages = math.ceil(total_records/size)
            
            paginate = {
            'content': data,
            'page_number': page,
            'page_size': size,
            'total_pages': total_pages,
            'total_records': total_records
            }
            
            return paginate



