from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy import NullPool
from app.config.config import settings



if settings.MODE == 'TEST':
    DATABASE_URL = settings.TEST_DATABASE_URL
    DATABASE_PARAMS = {"poolclass": NullPool}
else:
    DATABASE_URL = settings.DATABASE_URL
    DATABASE_PARAMS = {}

    # engine_switches = create_async_engine(settings.DATABASE_URL_SWITCHES, **DATABASE_PARAMS)
    # async_session_maker_switches = async_sessionmaker(engine_switches, expire_on_commit=True)

engine = create_async_engine(DATABASE_URL, **DATABASE_PARAMS)
async_session_maker = async_sessionmaker(engine, expire_on_commit=False)



class Base(DeclarativeBase):
    pass