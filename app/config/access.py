from app.config.flash import flash



async def site_access(request, current_user, access):
    if current_user == None:
        flash(request, 'Войдите на сайт для просмотра закрытых страниц.', 'alert-danger')
        return {'status': None, 'path': 'signin_get'}
    
    elif current_user.is_active == False:
        flash(request, 'Ваш логин не активен, обратитесь к администратору сайта.', 'alert-danger')
        return {'status': None, 'path': 'about_get'}
    
    elif current_user.is_admin == True:
        return {'status': True, 'path': None}
    
    elif not access in current_user.access.split(','):
        flash(request, 'Нет доступа к странице, обратитесь к администратору сайта.', 'alert-danger')
        return {'status': None, 'path': 'about_get'}
    
    return {'status': True, 'path': None}