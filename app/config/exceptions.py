from fastapi import HTTPException, status



class ZSTTKException(HTTPException):
    status_code = 500
    detail = ""
    
    def __init__(self):
        super().__init__(status_code=self.status_code, detail=self.detail)

class UserAlreadyExistsException(ZSTTKException):
    status_code=status.HTTP_409_CONFLICT
    detail="Пользователь уже существует"

        
class IncorrectEmailOrPasswordException(ZSTTKException):
    status_code=status.HTTP_401_UNAUTHORIZED
    detail="Неверная почта или пароль"
        
class TokenExpiredException(ZSTTKException):
    status_code=status.HTTP_403_FORBIDDEN
    detail="Срок действия токена истек. Войдите на сайт: http://192.168.174.131:8000/admin/login"



class TokenAbsentException(ZSTTKException):
    status_code=status.HTTP_401_UNAUTHORIZED
    detail="Зарегистрируйтесь"
        
class IncorrectTokenFormatException(ZSTTKException):
    status_code=status.HTTP_401_UNAUTHORIZED
    detail="Неверный формат токена"
        
class UserIsNotPresentException(ZSTTKException):
    status_code=status.HTTP_401_UNAUTHORIZED






